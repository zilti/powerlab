import * as utils from './utils.coffee'
import * as popup from './popup.coffee'

badges = (t) ->
  badges = []
  attachmentCount = t.card 'attachments'
                     .get 'attachments'
                     .filter (attachment) -> utils.isUrlGitlab(attachment.url)
                     .length
  if attachmentCount > 0
    badges.push({text: attachmentCount.toString()
                 icon: './images/card_logo.svg'})
  return badges

attachmentThumbnail = (t, options) ->
  if utils.isUrlGitlab(options.url)
    url:   options.url
    title: options.name
    image:
      url:  './images/attachment_logo.svg'
      logo: true
  else
    throw t.notHandled('Not a handled URL')

attachmentSections = (t, options) ->
  attachments = options.entries
  gitlabAttachments = attachments.filter(utils.isGitlabAttachment)
  if gitlabAttachments.length > 0
    [title:   'GitLab'
     icon:    './images/card_logo.svg'
     claimed: gitlabAttachments
     content:
       type: 'iframe'
       url:  t.signUrl './attachment.html']
  else
    []

buttons = () ->
  [icon:     './images/card_logo.svg'
   text:     'GitLab'
   callback: popup.openCard]

TrelloPowerUp.initialize(
  'authorization-status': utils.getAuthStatus
  'show-authorization': popup.openConfigure
  'card-buttons': cardButtons
  'card-badges': cardBadges
  'attachment-thumbnail': attachmentThumbnail
  'attachment-sections': attachmentSections
)
