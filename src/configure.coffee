import * as utils from './utils.coffee'
import * as project from './project.coffee'

apiInput = document.querySelector '#api-input'
tokenInput = document.querySelector '#token-input'
saveButton = document.querySelector '.save-btn'
autofillCheckmark = document.querySelector '#autofill-input'

toggleButton = () ->
  if apiInput.value.length > 0 and tokenInput.value.length > 0
    saveButton.removeAttribute 'disabled'
  else
    saveButton.setAttribute 'disabled', 'disabled'

saveCallback = (t, command) ->
  if command is 'callback'
    project.showProjects t
  else
    t.closePopup

saveCredentials = () ->
  t = TrelloPowerUp.iframe()
  command = t.args[0].context.command
  utils.setAuthToken t, apiInput.value, tokenInput.value
  .then () -> saveCallback t, command
  .catch (error) -> throw t.NotHandled 'there was an error trying to save the credentials.', error

validCredentialsCallback = (valid) ->
  if valid
    saveCredentials()
  else
    if not autofillCheckmark.checked
      apiInput.parentElement.classList.add 'invalid'
    tokenInput.parentElement.classList.add 'invalid'
    saveButton.classList.remove 'saving'
    saveButton.innerText = 'Save'
    saveButton.removeAttribute 'disabled'

save = () ->
  saveButton.classList.add 'saving'
  saveButton.innerText = 'Saving'
  saveButton.setAttribute 'disabled', 'disabled'
  api.credentialsValid apiInput.value, tokenInput.value
  .then validCredentialsCallback
  .then utils.sizeContainer

removeInvalid = () ->
  if this.parentElement.classList.contains 'invalid'
    this.parentElement.classList.remove 'invalid'

toggleAutofill = () ->
  if this.checked
    window.localStorage.setItem 'cached-api', apiInput.value
    apiInput.value = 'https://gitlab.com/api/v4'
    apiInput.setAttribute 'disabled', 'disabled'
  else
    apiInput.value = window.localStorage.getItem('cached-api') or ''
    apiInput.removeAttribute 'disabled'
  removeInvalid.call apiInput

apiInput.addEventListener 'input', removeInvalid
apiInput.addEventListener 'input', toggleButton
tokenInput.addEventListener 'input', removeInvalid
tokenInput.addEventListener 'input', toggleButton
saveButton.addEventListener 'click', save
autofillCheckmark.addEventListener 'change', toggleAutofill
