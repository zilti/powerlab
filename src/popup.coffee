openConfigure = (t) ->
  t.popup title:  'Configure',
          url:    './configure.html',
          height: 470

openAuthorize = (t) ->
  t.popup title:  'Authorize',
          url:    'authorize.html',
          height: 195

selectProject = (t, projects) ->
  t.popup title:  'Projects',
          items:  projects,
          search:
            count:       10
            placeholder: 'Search Projects'
            empty:       'No projects found'

selectMergeRequest = (t, mergeRequests) ->
  t.popup title:  'Merge Requests',
          items:  mergeRequests,
          search: count:       10
                  placeholder: 'Search Merge Requests'
                  empty:       'No merge requests found'

selectRecentCommit = (t, commits) ->
  t.popup title:  'Recent Commits',
          items:  commits,
          search: count:       10
                  placeholder: 'Search Recent Commits'
                  empty:       'No recent commits found'

openCard = (t) ->

  t.popup title: 'GitLab',
          items: [{text: 'Attach Merge Request', callback: mergeRequest.attach},
                  {text: 'Attach Commit', callback: commit.attach}]

export { openConfigure, openAuthorize, selectProject, selectMergeRequest, selectRecentCommit, openCard }
