import * as axios from 'https://unpkg.com/axios/dist/axios.min.js'

getAllProjects = (auth) ->
  axios.get(
    "#{auth.api}/projects"
    {params: {private_token:               auth.token
              membership:                  true
              with_merge_requests_enabled: true
              simple:                      true
              per_page:                    100
              order_by:                    'last_activity_at'}})

getAllMergeRequests = (auth, projectId) ->
  axios.get(
    "#{auth.api}/projects/#{projectId}/merge_requests"
    {params: {private_token: auth.token
              view:          'simple'
              per_page:      100}})

getRecentCommits = (auth, projectId) ->
  axios.get(
    "#{auth.api}/projects/#{projectId}/repository/commits"
    {params: {private_token: auth.token}})

credentialsValid = (endpoint, token) ->
  axios.get(
    "#{endpoint}/version"
    {params: {private_token: token}})
    .then () -> true
    .catch () -> false

export { getAllProjects, getAllMergeRequests, getRecentCommits, credentialsValid }
