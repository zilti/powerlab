parseURL = (href) ->
  tag = document.createElement 'a'
  tag.href = href
  return tag

authCallback = (resolve, auth) ->
  apiExists = auth and typeof auth.api isnt 'undefined'
  tokenExists = auth and typeof auth.token isnt 'undefined'
  resolve({authorized: apiExists and tokenExists})

authError = (t, error) ->
  throw t.NotHandled('An error occurred while getting authorization status from Trello', error)

setAuthToken = (t, api, token) ->
  t.set('member'
        'private'
        'auth'
        {api: api, token: token})

getAuthToken = (t) ->
  t.get('member', 'private', 'auth')

getAuthStatus = (t) ->
  new TrelloPowerUp.Promise (resolve) ->
    getAuthToken(t)
    .then authCallback.bind(this, resolve)
    .catch authError.bind(this, t)

isUrlGitlab = (url) ->
  parseURL(url).hostname is 'gitlab.com'

isMergeRequest = (attachment) ->
  !!attachment.url.match(/\/merge_requests\/\d+$/m)

isCommit = (attachment) ->
  !!attachment.url.match(/\/commit\/[a-z0-9]+$/m)

getAttachmentType = (attachment) ->
  if isMergeRequest attachment
    'merge-request'
  else if isCommit attachment
    'commit'
  else
    'you-broke-it'

isGitlabAttachment = (attachment) ->
  isUrlGitlab attachment.url and getAttachmentType attachment isnt 'you-broke-it'

sizeContainer = () ->
  TrelloPowerUp.iframe().sizeTo('#content')

export { parseURL, getAuthStatus, isGitlabAttachment, isUrlGitlab, sizeContainer, setAuthToken, getAuthToken, getAttachmentType }
