import * as api from './api.coffee'
import * as utils from './utils.coffee'

getProjects = (t) ->
  utils.getAuthToken t
    .then api.getAllProjects
    .catch (error) => throw t.NotHandled 'There was an error trying to fetch the projects via GitLab API.', error

mapProjects = (selectCallback, project) ->
  {text:     project.name_with_namespace
   callback: (t) -> selectCallback t, project}

showProjects = (t, selectCallback) ->
  getProjects t
    .then (response) => mapProjects selectCallback, project for project in response.data
    .then (projects) => popup.openProjects t, projects
    .catch (error) => throw t.NotHandled 'There was an error trying to display the projects.', error

export { showProjects }
