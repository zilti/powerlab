import * as utils from './utils.coffee'
t = TrelloPowerUp.iframe();

appendLink = (attachment) ->
  document
  .getElementById 'content'
  .appendChild <a className="gl-link #{linkType} quiet-button" href={attachment.url} target="_blank">{attachment.name}</a>

t.render(() ->
  t.card 'attachments'
  .get 'attachments'
  .filter utils.isGitlabAttachment
  .then ((gitlabAttachments) ->
    document.getElementById('content').innerHTML = ''
    appendLink attachment for attachment in gitlabAttachments)
  .then utils.sizeContainer)
