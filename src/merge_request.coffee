import * as popup from './popup.coffee'
import * as api from './api.coffee'
import * as project from './project.coffee'

attachMergeRequest = (t, url, name) ->
  t.attach {url: url, name: name}
  .then () -> t.closePopup
  .catch (error) -> throw t.NotHandled 'There was an error trying to attach the merge request to the card', error

getMergeRequests = (t, projectId) ->
  utils.getAuthToken t
  .then (auth) -> api.getAllMergeRequests auth, projectId
  .catch (error) -> throw t.NotHandled 'There was an error trying to fetch the merge requests from GitLab API', error

mapMergeRequest = (t, projectNamespace, mr) ->
  cardTitle = "#{mr.title} (!#{mr.iid}) · #{projectNamespace}"
  text:     "!#{mr.iid} #{mr.title}"
  callback: (trelloInstance) -> attachMergeRequest trelloInstance, mr.web_url, cardTitle

showProjectMergeRequest = (t, project) ->
  getMergeRequests t, project.id
  .then (response) -> mapMergeRequest t, project.name_with_namespace, mr for mr  in response
  .then (mergeRequests) -> popup.openMergeRequests t, mergeRequests
  .catch (error) -> t.NotHandled 'There was an error trying to get the merge requests from GitLab API', error

getAuth = (t, authStatus) ->
  if authStatus and authStatus.authorized
    project.showProjects t, showProjectMergeRequest
  else
    popup.openConfigure t

attach = (t) ->
  utils.getAuthStatus t
  .then (authStatus) -> getAuth t, authStatus

export { attach }
